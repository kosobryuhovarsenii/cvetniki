$(function () {
    if ($('#checkbox').is(':checked')){
        //alert('Включен');
    } else {
        //alert('Выключен');
    }
    $(document).on('click', '.products-block .radio-block label.filter_sort_label', function () {
        var order, sort;
        sort = $(this).data('sort');
        order = $(this).data('order');

        $.ajax({
            type: "POST",
            url: '/ajax/sort.php',
            data: {
                ORDER: order,
                SORT: sort
            },
            success: function (data) {
                location.reload();
            }
        });
    });
});