(function($) {

// Инпуты кол-ва
(function() {
	var onclick = function() {
		var $input = $(this).siblings('.number-input-input');
		var newValue = Number($input.val()) + (this.innerHTML == '-' ? -1 : 1);
		if (newValue >= Number($input.prop('min')))
			$input.val(newValue);
		$input.focus();
		return false;
	};
	var $element = $('.number-input').each(function() {
		var $buttons = $(this).find('.number-input-button');
		$buttons.on('mousedown', onclick);
	});
})();

// Обратный отсчет акционных товаров
$('#hot-product-scroller [data-countdown]').each(function() {
	var $this = $(this);
	var date = new Date($this.data('countdown'));
	var countdown = Math.max(0, date.getTime() - Date.now()) / 1000;

	var timer = new CountDownTimer(countdown);

	timer.onTick(format).start();

	function format(days, hours, minutes, seconds) {
		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;
		hours = hours < 10 ? "0" + hours : hours;
		days = days < 10 ? "0" + days : days;
		$this.text(days + ' : ' + hours + ' : ' + minutes + ' : ' + seconds);
	}
});

// Скроллеры
$('.home-slider').slick({
	autoplay:true,
	autoplaySpeed:5000,
});

var productScrollerOptions = {
	infinite: false,
	slidesToShow: 4,
	slidesToScroll: 1,
	arrows: true,
	dots: true,
	responsive: [{
		breakpoint: 1299,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
		}
	},{
		breakpoint: 991,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
		}
	}],
	customPaging: function(slider, i) {
		if (slider.getDotCount() < 1)
			return $();
		return $('<button type="button" />').text(i + 1);
	}
};

$('.product-scroller').slick(productScrollerOptions).data('slick-options', productScrollerOptions);

var reviewsScrollerOptions = {
	infinite: false,
	slidesToShow: 3,
	slidesToScroll: 3,
	arrows: true,
	dots: false,
	responsive: [{
		breakpoint: 1299,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
		}
	},{
		breakpoint: 991,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
		}
	}]
};
$('.reviews-scroller').slick(reviewsScrollerOptions).data('slick-options', reviewsScrollerOptions);



$('.our-clients').slick({
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  centerMode: false,
	  variableWidth: true
}).css({overflow: 'visible'});

if (window.innerWidth < 992) { // на узких экранах все делаем прокручиваемыми
	$('.selectable-tags').slick({
		  infinite: false,
		  speed: 300,
		  slidesToShow: 1,
		  centerMode: false,
		  variableWidth: true
	}).css({overflow: 'visible'});
} else { // на широких экранах прокручиваем только заведомо длинные списки тегов
	$('.selectable-tags-scrollable').slick({
		  infinite: false,
		  speed: 300,
		  slidesToShow: 1,
		  centerMode: false,
		  variableWidth: true
	}).css({overflow: 'visible'});
}

// Теги (фильтры) для скроллеров товаров
$('.selectable-tags').each(function() {
	var $filterTarget = $('#' + $(this).data('filter-target'));

	var $elements = $filterTarget.find('[data-type]');

	$(this).find('a').click(function() {
		var filter = $(this).data('filter');
		$filterTarget.slick('unslick').find('[data-type]').detach();
		if (filter) {
			$elements.filter(function() {
				return $(this).data('type') == filter;
			}).appendTo($filterTarget);
		} else {
			$elements.appendTo($filterTarget);
		}
		$filterTarget.slick($filterTarget.data('slick-options'));
		$(this).addClass('active').siblings().removeClass('active');
	});

	if ($(this).find('.active').data('filter')) {
		$(this).find('.active').click();
	}
});


$('.js-menu-toggle').click(function() {
	$('.navbar-nav').toggleClass('mobile-open');
});

$('.popup-control-wrap button').click(function() {
	$(this).toggleClass('button-toggle-open').next().toggleClass('open');

	if ($(this).hasClass('button-toggle-open')) $('.overlay').show().addClass('transparent');
});

$('.popup-menu-section').click(function() {
	$('.popup-control-wrap button').each(function (index, element) {
		$(this).removeClass('button-toggle-open').next().removeClass('open');
	});
	$('.overlay').hide().removeClass('transparent');
});
$('.sidebar-menu > li').hover(function() {
	$(this).addClass('active').siblings().removeClass('active');
}, function() {
	$(this).removeClass('active');
});


$('.sidebar-menu-popup-list > li').hover(function() {
	$(this).addClass('active').siblings().removeClass('active');
});




})(jQuery);
