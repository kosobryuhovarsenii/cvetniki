jQuery(function ($) {
    var minVal = parseFloat($('.sliderRangeSettings input[name="minPrice"]').val());
    var maxVal = parseFloat($('.sliderRangeSettings input[name="maxPrice"]').val());
    var curMinVal = parseFloat($('.sliderRangeSettings input[name="curMinPrice"]').val());
    var curMaxVal = parseFloat($('.sliderRangeSettings input[name="curMaxPrice"]').val());

    if (!curMinVal) curMinVal = minVal;
    if (!curMaxVal) curMaxVal = maxVal;

    $('#rangeslider').slider({
        min: minVal,
        max: maxVal,
        values: [curMinVal, curMaxVal],
        range: true,
        create: setInputsFrom2Slider,
        slide: setInputsFrom2Slider,
        stop: setInputsFrom2Slider
    })

    function setInputsFrom2Slider() {
        $('#arrFilter_P1_MIN').val($('#rangeslider').slider("values", 0));
        $('#arrFilter_P1_MAX').val($('#rangeslider').slider("values", 1));
    }

    $('input').change(function (e) {
        switch (this.id) {
            case "arrFilter_P1_MIN":
            case "arrFilter_P1_MAX":
                var index = (this.id == "arrFilter_P1_MAX") ? 1 : 0;
                $('#rangeslider').slider("values", index, $(this).val())
                break;
        }
    });

    $('.interval_price input').on('change', function(){
        var link = $(this).attr('data-url');
        location.href = link;
    });
});
jQuery(function ($) {
    $('.rangeslider2').each(function () {
        var this_slider = $(this);
        var filter_parent = $(this).parent('.filter-interval');
        var sliderSettings = $('.sliderRangeSettings', filter_parent);

        var minVal = parseFloat($('input[name=minVal]', sliderSettings).val());
        var maxVal = parseFloat($('input[name=maxVal]', sliderSettings).val());
        var curMinVal = parseFloat($('input[name=curMinVal]', sliderSettings).val());
        var curMaxVal = parseFloat($('input[name=curMaxVal]', sliderSettings).val());

        if (!curMinVal) curMinVal = minVal;
        if (!curMaxVal) curMaxVal = maxVal;

        $(this_slider).slider({
            min: minVal,
            max: maxVal,
            values: [curMinVal, curMaxVal],
            range: true,
            create: setInputsFrom2Slider,
            slide: setInputsFrom2Slider,
            stop: setInputsFrom2Slider
        });

        function setInputsFrom2Slider() {
            $('input.minVal', filter_parent).val($('.rangeslider2').slider("values", 0));
            $('input.maxVal', filter_parent).val($('.rangeslider2').slider("values", 1));
        }

        $('input').change(function (e) {
            switch (this.id) {
                case "rangeMin2":
                case "rangeMax2":
                    var index = (this.id == "rangeMax2") ? 1 : 0;
                    $(this_slider).slider("values", index, $(this).val())
                    break;
            }
        });
    });
});

jQuery(function ($) {

    $(function () {
        $('.catalog-menu-block li, .products-block .filter-cat').click(function () {
            $(this).toggleClass('active');
            $('input', $(this)).trigger('click');
        });
        $('.products-block .filter-sort span, .products-block .filter-group span').click(function () {
            $(this).parent().toggleClass('active');
        });
        $('.clear-cat i').click(function () {
            $('.custom-cats-block, .clear-cat').toggleClass('active');
        });
        $('.products-block .filter-sort .radio-block label, .products-block .filter-group .radio-block label, .clear-cat a').click(function () {
            $(this).parent().parent().removeClass('active');
        });
        $(document).on('mouseup', function (e) {
            var div = $('.products-block .filter-sort, .products-block .filter-group');
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                div.removeClass('active');
            }
        });
        $('.products-block .filter-view-var img').click(function () {
            $('.products-block .filter-view-var img').removeClass('active');
            $(this).addClass('active');
        });
        $('.product-custom-nav .num span').click(function () {
            $('.product-custom-nav .num span').removeClass('active');
            $(this).addClass('active');
        });
        $('.apply-link-filter a').on('click', function () {
            $(this).closest('form').submit();
        });

    });

    /* $(document).ready(function () {
         $(document).on('click', '.catalog-menu-block li a', function (e) {
             e.preventDefault();
         });
     });*/

    $('.minus').on('click', function (e) {
        e.preventDefault();
        var countProd = parseInt($(this).siblings('input').val());
        --countProd;
        if (countProd < 1) {
            countProd = 1;
        }
        $(this).siblings('input').val(countProd);
        $(this).parents('.btns-block').find('.cart a').data('quantity',countProd);

    });

    $('.plus').on('click', function (e) {
        e.preventDefault();
        var countProd = parseInt($(this).siblings('input').val());
        countProd++;
        $(this).siblings('input').val(countProd);
        $(this).parents('.btns-block').find('.cart a').data('quantity',countProd);

    });
    $(document).on('keyup',".btns-block .quantity input",function () {
        var countProd = parseInt($(this).val());
        $(this).parents('.btns-block').find('.cart a').data('quantity',countProd);
    });

    $(document).ready(function () {
        $('.filter-interval .inputDiv input').bind("change keyup input click", function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
    });

    $('.recommended-custom-slider, .related-custom-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
        arrows: true,
        autoplay: false,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    //    $('.accordeon .acc-head').click(function () {
    //        $('.accordeon .acc-head').removeClass('active');
    //        $(this).toggleClass('active');
    //        $('.accordeon .acc-body').not($(this).next()).slideUp(300);
    //        $(this).next().slideToggle(300);
    //    })

    $('.accordeon .acc-head').click(function () {
        $('.accordeon .acc-head').not($(this)).removeClass('active');
        $(this).toggleClass('active');
        $('.accordeon .acc-body').not($(this).next()).slideUp(300);
        $(this).next().slideToggle(300);
    });

    $('.catalog-menu-block .menu-block-title').click(function () {
        $('.catalog-menu-block .menu-block-title').not($(this)).removeClass('active');
        $(this).toggleClass('active');
        $('.catalog-menu-block .menu-block-content').not($(this).next()).slideUp(300);
        $(this).next().slideToggle(300);
    });

    $('.catalog-menu-block .menu-block-content li label').click(function () {
        $(this).toggleClass('active');
    });

    $(function () {
        $('.tabs__titles').on('click', '.tabs__title:not(.active)', function () {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.reviews-tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });
    });

    $('.rev-slider-all, .rev-slider-photo, .rev-slider-video, .rev-slider-text, .rev-slider-audio, .rev-slider-yandex').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
});


//(function ($) {
//	$('.swipebox').swipebox({
//		hideBarsDelay: 0, 
//		loopAtEnd: true, 
//		removeBarsOnMobile: false
//	});
//})(jQuery);

var audio;
window.onload = function () {
    audio = document.getElementById("audioPlayer");
};

function play() {
    audio.play();
}

function stop() {
    audio.pause();
    audio.currentTime = 0;
}

jQuery(function ($) {
    $('.audio-custom .btn').click(function () {
        $('.audio-custom .btn').removeClass('active');
        $(this).siblings().addClass('active');
    })

    $(function () {
        $('.mobile-filter-custom').click(function () {
            $('body').addClass('mobile-filter-open');
        })
        $('.mobile-filter-close span').click(function () {
            $('body').removeClass('mobile-filter-open');
        })
    });
    
    $('.faq-custom .faq-more a').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.faq-custom .faq-custom-hidden').slideToggle(300);
    })

});

jQuery(function ($) {
    $('.products-block .product-custom .image').matchHeight();
    //    $('.products-block .product-custom .title').matchHeight();
//    $('.products-block .product-custom .price-custom').matchHeight();
    $('.why-we .title-custom').matchHeight();
    $('.product-custom-inner .image').matchHeight();
    $(function () {
        $('.reviews-tabs').on('setPosition', function () {
            $(this).find('.slick-slide .review-custom').height('auto');
            var slickTrack = $(this).find('.slick-track');
            var slickTrackHeight = $(slickTrack).height();
            $(this).find('.slick-slide .review-custom').css('height', slickTrackHeight + 'px');
        });
        $('.reviews-tabs').on('setPosition', function () {
            $(this).find('.slick-slide .review-custom .review-content').height('auto');
            var slickTrack = $(this).find('.slick-slide .review-custom .review-content');
            var slickTrackHeight = $(slickTrack).height();
            $(this).find('.slick-slide .review-custom .review-content').css('height', slickTrackHeight + 'px');
        });
//        $('.related-custom-slider').on('setPosition', function () {
//            $(this).find('.slick-slide .price-custom-inner').height('auto');
//            var slickTrack = $(this).find('.slick-slide .price-custom-inner');
//            var slickTrackHeight = $(slickTrack).height();
//            $(this).find('.slick-slide .price-custom-inner').css('height', slickTrackHeight + 'px');
//        });
    })
});

$(document).ready(function () {
    $('form.subscribe_left').on('submit', function () {
        var action = $(this).attr('action');
        var form_data = '';
        var form_error = false;
        $('input[type=text]', $(this)).each(function () {
            var name = $(this).attr('name');
            var val = $(this).val();
            if (!val) {
                form_error = true;
                $(this).addClass('error');
            }
            form_data += name + '=' + val + '&';
        });

        $('input[type=email]', $(this)).each(function () {
            var name = $(this).attr('name');
            var val = $(this).val();
            if (!val) {
                form_error = true;
                $(this).addClass('error');
            }
            form_data += name + '=' + val + '&';
        });

        if (!$('input[name=private_policy]').prop('checked')) {
            form_error = true;
            $('form.subscribe_left .accept').addClass('error');
        }

        if (!form_error) {
            $.ajax({
                type: 'POST',
                url: action,
                data: form_data,
                success: function (res) {
                    $('form.subscribe_left').html(res);
                }
            });
        }

        return false;
    });
});
