function SmartFilter() {
    this._debounceTimeout = 500;

    this._filter = {
        name: '',
        values: ''
    };

    this._currecntFilter = {
        name: '',
        values: ''
    };

    this.selectors = {
        smartFilter: '.js-smart-filter',
        input: '.js-filter__input',
        reset: '.js-filter-reset',
        submit: '.js-filter__submit'
    };

    this._componentParams = $(this.selectors.smartFilter).data('componentParams');

    this.filterValues = {};

    this.filter(true);

    this.initEvents();
}

SmartFilter.prototype.getFilter = function () {
    return this._currecntFilter;
};

SmartFilter.prototype.initEvents = function () {
    var that = this;
    $(document).on('change', this.selectors.input, $.debounce(that._debounceTimeout, this.filter.bind(this)));

    $(document).on('click touch', this.selectors.submit, function () {
        window.location = that.filterData.FILTER_URL;
        return false;
    });
};

SmartFilter.prototype.filter = function (firstLaunch) {
    var filterValues = this.getFilterValues();
    AjaxModule.requestComponent({'params': this._componentParams}, filterValues, function (data) {
        this.filterData = data.result;
        this._filter = data.filter;

        if (firstLaunch) {
            this._currecntFilter = this._filter;
        }



    }.bind(this));
};

SmartFilter.prototype.getFilterValues = function () {
    var filterValues = {
        ajax: 'y'
    };
    $(this.selectors.input).each(function () {
        if ($(this).is(":checkbox") || $(this).is(':radio')) {
            if ($(this).is(':checked')) {
                filterValues[$(this).attr('name')] = $(this).val();
            }
        } else {
            filterValues[$(this).attr('name')] = $(this).val();
        }
    });

    return filterValues;
};