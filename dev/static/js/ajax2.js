var AjaxModule = (function () {

    var _url = '/local/components/astdesign/ajax.util/ajax.php';
    var binded = false;

    var _send = function (data, method, callback) {
        $.ajax({
            method: method,
            url: _url,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response['success'] === true) {
                callback(response['data']);
            } else {
                console.error('Fail Ajax Request');
            }
        }).fail(function (e, b, error) {
            //console.error(error);
        });
    };

    return {
        requestComponent: function (componentParameters, params, callback) {
            if (typeof params === 'undefined') {
                params = {};
            }

            params['parameters'] = componentParameters['params'];
            params['component'] = componentParameters['component'];
            params['requestType'] = 'component';

            _send(params, 'POST', callback);
        },
        requestApi: function (action, params, callback) {
            if (typeof params === 'undefined') {
                params = {};
            }

            params['action'] = action;
            _send(params, 'POST', callback);
        },
        bindBaseEvents: function () {
            this.bindAddToBasket();
            this.bindAddToBasketNew();
        },
        bindPagination: function () {
            if (this.binded === false || typeof this.binded === 'undefined') {
                $(document).on('click', '.js-show-more', function () {
                    //alert('bindPagination');
                    var $navigation = $(this).closest('.js-navigation');
                    var dataContainer = $navigation.attr('data-container');
                    if (typeof dataContainer !== 'undefined') {
                        $container = $('.js-products-container[data-container="' + dataContainer + '"]');
                    } else {
                        var $container = $navigation.siblings('.js-products-container');
                        if (!$container.length) {
                            $container = $('.js-products-container');
                        }
                    }
                    var componentKey = $navigation.data('componentParams');
                    var pageNum = Math.max(1, $navigation.find('.pagination-block-item_current').text() ^ 0) + 1;
                    var size = $navigation.find('.pereference-view__item_active').data('size');

                    $navigation.find('.preloader').show();
                    $(this).hide();
                    AjaxModule.requestComponent({params: componentKey}, {
                        PAGEN_1: pageNum,
                        SIZEN_1: size,
                        filter: SF.getFilter()
                    }, function (response) {
                        if ($container.find('.js-products-container').length) {
                            $container.find('.js-products-container').append(response.items);
                        } else {
                            $container.append(response.items);
                        }
                        $navigation.html(response.pagination);
                        AjaxModule.setCurrentPrices();
                    });

                    return false;
                });
                this.binded = true;
            }
        },
        bindPaginationNew: function () {
            if (this.binded === false || typeof this.binded === 'undefined') {
                $(document).on('click', '.js-show-more', function () {
                    //alert('bindPaginationNew');
                    var $navigation = $(this).closest('.js-navigation');
                    var dataContainer = $navigation.attr('data-container');
                    if (typeof dataContainer !== 'undefined') {
                        $container = $('.js-products-container[data-container="' + dataContainer + '"]');
                    } else {
                        var $container = $navigation.siblings('.js-products-container');
                        if (!$container.length) {
                            $container = $('.js-products-container');
                        }
                    }
                    var componentKey = $navigation.data('componentParams');
                    var pageNum = Math.max(1, $navigation.find('.pagination-block-item_current').text() ^ 0) + 1;
                    var size = $navigation.find('.pereference-view__item_active').data('size');

                    $navigation.find('.preloader').show();
                    $(this).hide();
                    AjaxModule.requestComponent({params: componentKey}, {
                        PAGEN_1: pageNum,
                        SIZEN_1: size,
                        filter: SF.getFilter()
                    }, function (response) {
                        console.log(response);
                        if ($container.find('.js-products-container').length) {
                            $container.find('.js-products-container').append(response.items);
                        } else {
                            $container.append(response.items);
                        }
                        $navigation.html(response.pagination);
                        AjaxModule.setCurrentPrices();
                    });

                    return false;
                });
                this.binded = true;
            }
        },
        bindAddToBasket: function () {
            $(document).on('click', '.js-add2basket', function () {
                var $that = $(this);
                var $parent = $that.closest('.product-item');
                if ($parent.length) {
                    productObj = {
                        name: $parent.find('.product-item-title').text(),
                        id: $parent.attr("data-product-id"),
                        price: parseFloat($parent.find(".price-default").text().replace(/[^\d.]/g, '')),
                        quantity: 1
                    };
                }
                var $parent = $that.closest('.card-item');
                if ($parent.length) {
                    productObj = {
                        name: $parent.find('.item-title').text(),
                        id: $parent.attr("data-product-id"),
                        price: parseFloat($parent.find(".item-price").text().replace(/[^\d.]/g, '')),
                        quantity: 1
                    };
                }
                var $parent = $that.closest('.item-popular');
                if ($parent.length) {
                    productObj = {
                        name: $parent.find('.item-popular-content__title').text(),
                        id: $that.attr("data-id"),
                        price: parseFloat($parent.find(".item-price_norm").text().replace(/[^\d.]/g, '')),
                        quantity: parseInt($parent.find(".js-product-quantity").val())
                    };
                }
                var $parent = $that.closest('.product-content');
                if ($parent.length) {
                    productObj = {
                        name: $('h1.product-title').text(),
                        id: $('.js-product-prices').attr("data-id"),
                        price: parseFloat($('.list-item-content-price__item_active .item-price_norm').text().replace(/[^\d.]/g, '')),
                        quantity: parseInt($(".product-content .js-product-quantity").val())
                    };
                }
                /* if (productObj) {
                    productObj.name = productObj.name.trim();
                    dataLayer.push({
                        'event': 'addToCart',
                        'ecommerce': {
                            'currencyCode': 'RUB',
                            'add': {
                                'products': [{
                                    'name': productObj.name,
                                    'id': productObj.id,
                                    'price': productObj.price,
                                    'quantity': productObj.quantity
                                }]
                            }
                        }
                    });
                } */
                AjaxModule.addToBasket($(this).data('id'), $(this).data('quantity'));
                return false;
            });
        },
        bindAddToBasketNew: function () {
            $(document).on('click', '.js-add2basketNew', function () {
                var $that = $(this);
				var $parent = $that.closest('.product-custom');
                if ($parent.length) {
                    productObj = {
                        name: $parent.find('.title a').text(),
                        id: $parent.attr("data-product-id"),
                        price: parseFloat($parent.find(".price-custom-inner span").text().replace(/[^\d.]/g, '')),
                        quantity: 1
                    };
                }
                AjaxModule.addToBasket($(this).data('id'), $(this).data('quantity'));
                return false;
            });
        },
        addToBasket: function (productId, quantity) {
            var $basket = $($('.js-header-cart')[0]);
            var componentParams = $basket.data('component');
            AjaxModule.requestComponent(componentParams, {
                id: productId,
                quantity: quantity,
                action: 'add'
            }, function (response) {
                $('.js-basket-price').html(response['totalPrice']);
                $('.js-basket-count').html(response['totalCount']);
                if (parseInt(response['totalCount']) <= 0) {
                    $('.js-basket-count').hide();
                } else {
                    $('.js-basket-count').show();
                }
                AjaxModule.setCurrentPrices();
                AjaxModule.showOrderForm();
            });
        },
        setCurrentPrices: function () {
            var $priceBlocks = {};
            var productIds = [];
            $('.js-product-prices').each(function () {
                var id = $(this).data('id');
                productIds.push(id);
                $priceBlocks[id] = $(this)
            });

            if (productIds.length) {
                this.requestApi('getCurrentPrices', {productId: productIds}, function (response) {
                    for (var id in $priceBlocks) {
                        var priceId = response[id] ? response[id] : 1;
                        $priceBlocks[id].each(function () {
                            $(this).find('.js-price-row').each(function () {
                                if (($(this).data('priceId') ^ 0) === priceId) {
                                    $(this).addClass('list-item-content-price__item_active');
                                } else {
                                    $(this).removeClass('list-item-content-price__item_active');
                                }
                            });
                        });
                    }
                });
            }
        },
        showOrderForm: function () {
            $('.popup').remove();
            page = '/local/ajax/popup-order.php';
            $.get(page, function (data) {
                $('.header').not('.header_white_dynamic').append(data);
                $('input, select').not('.star-rating, .review-rating').styler();
                jsPhone();

                if ($('.popup').length) {
                    topPos = $(window).scrollTop() + ($(window).height() - $('.popup').height()) / 2;
                    $('.popup').css('top', topPos + 'px');
                }

                if ($('.selected-flag').length) {
                    $('.selected-flag').removeAttr('tabindex');
                }

            });
            $('.overlay').fadeIn(200);
        }
    }
})();

AjaxModule.bindBaseEvents();
$(function () {
    AjaxModule.setCurrentPrices();
});