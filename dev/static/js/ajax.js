function locationCityOnSelect(locationID, reload) {
    if (typeof reload == 'undefined') reload = true;
    $.ajax({
        type: "POST",
        url: '/local/ajax/locations.php',
        data: {locationID: locationID},
        async: false,
        dataType: "json",
        success: function (response) {
            if (response.status == 'ok' && reload) {
                location.reload();
            }
        }
    });
}

$(document).ready(function () {

    $(document).on('change', '.city-list input[name=city]', function () {
        locationCityOnSelect($(this).val());
    });

    $(document).on('submit', '#form-individual', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Данные отправлены',
                            text: 'Спасибо за доверие',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });


    $(document).on('submit', '#form-callback', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.js-popupClose').click();
                        swal({
                            type: 'success',
                            title: 'Данные отправлены',
                            text: 'Спасибо за доверие',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        yaCounter45065708.reachGoal('callbackSend');
                        yaCounter45065708.reachGoal('globalConversion');
                        ga('send', 'event', 'main', 'callbackSend');
                        ga('send', 'event', 'main', 'globalConversion');
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                }
            });
        }

        return false;
    });


    $(document).on('submit', '#form-organization', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Данные отправлены',
                            text: 'Спасибо за доверие',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });


    $(document).on('submit', '#form-login', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.js-popupClose').click();
                        document.location.reload();
                    } else {
                        swal({
                            type: 'error',
                            title: response.title,
                            text: response.message,
                            timer: 5000,
                            showConfirmButton: true
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });

    $(document).on('submit', '#form-password-recovery', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.js-popupClose').click();
                        swal({
                            type: 'success',
                            title: response.title,
                            text: response.message,
                            timer: 5000,
                            showConfirmButton: false
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.title,
                            text: response.message,
                            timer: 5000,
                            showConfirmButton: true
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });

    $(document).on('submit', '#form-password-recovery-step2', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.js-popupClose').click();
                        swal({
                            type: 'success',
                            title: response.title,
                            text: response.message,
                            timer: 3000,
                            showConfirmButton: false,
                            onClose: function () {
                                document.location = '/personal/'
                            }
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.title,
                            text: response.message,
                            timer: 5000,
                            showConfirmButton: true
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });


    $(document).on('submit', '#form-banner', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Данные отправлены',
                            text: 'Спасибо за доверие',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });


    $(document).on('submit', '#form-coupon', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Данные отправлены',
                            text: 'Спасибо за доверие :)',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });

    $(document).on('submit', '#form-addreview', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.js-popupClose').click();
                        swal({
                            type: 'success',
                            title: 'Заявка успешно отправлена',
                            text: 'Скоро мы свяжемся с вами',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                },
            });
        }

        return false;
    });


    $(document).on('submit', '#form-needhelp', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Заявка успешно отправлена',
                            text: 'Скоро мы свяжемся с вами',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        yaCounter45065708.reachGoal('needhelpSend');
                        yaCounter45065708.reachGoal('globalConversion');
                        ga('send', 'event', 'main', 'needhelpSend');
                        ga('send', 'event', 'main', 'globalConversion');
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                }
            });
        }

        return false;
    });

    $(document).on('submit', '#provide-form', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        formData = new FormData(form.get(0));
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Заявка успешно отправлена',
                            text: 'Скоро мы свяжемся с вами',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        yaCounter45065708.reachGoal('dealersSend');
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                }
            });
        }

        return false;
    });

    $(document).on('submit', '#form-support', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Сообщение отправлено',
                            text: 'Мы обязательно ответим на ваш вопрос',
                            showConfirmButton: false,
                            timer: 3000,
                            onClose: function () {
                                $('.js-popupClose').trigger('click');
                            }
                        })
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                }
            });
        }

        return false;
    });

    $(document).on('submit', '.comments-form', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        swal({
                            type: 'success',
                            title: 'Спасибо',
                            text: 'Ваш комментарий был успешно отправлен',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        yaCounter45065708.reachGoal('newComment');
                        yaCounter45065708.reachGoal('newComment');
                    } else {
                        swal({
                            type: 'error',
                            title: 'Произошла ошибка',
                            text: response.statusText,
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                }
            });
        }

        return false;
    });


    $(document).on('submit', '#form-buy-fast,#form-buy-fast2', function () {
        var form = $(this);
        var url = $(this).attr('data-ajax-url');
        var id = $(this).attr('id');
        if (!form.find('.notvalidate').length) {
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.js-popupClose').click();
                        swal({
                            type: 'success',
                            title: 'Данные отправлены',
                            text: 'Спасибо за доверие',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        if (id == 'form-buy-fast') {
                            yaCounter45065708.reachGoal('buyFastProductSend');
                            yaCounter45065708.reachGoal('globalConversion');
                            ga('send', 'event', 'main', 'buyFastProductSend');
                            ga('send', 'event', 'main', 'globalConversion');
                        }
                        if (id == 'form-buy-fast2') {
                            yaCounter45065708.reachGoal('buyFastSectionSend');
                            yaCounter45065708.reachGoal('globalConversion');
                            ga('send', 'event', 'main', 'buyFastSectionSend');
                            ga('send', 'event', 'main', 'globalConversion');
                        }
                    } else {
                        swal({
                            type: 'error',
                            title: response.message,
                            text: 'Повторите еще раз',
                            timer: 3000,
                            showConfirmButton: false
                        })
                    }
                },
                error: function (xhr, str) {
                    swal({
                        type: 'error',
                        title: 'Ошибка :(',
                        text: 'Повторите еще раз',
                        timer: 3000,
                        showConfirmButton: false
                    })
                }
            });
        }

        return false;
    });

    /*$(document).on('submit', '.js-form-actions', function () {
     var form = $(this);
     var url = $(this).attr('data-ajax-url');
     if (!form.find('.notvalidate').length) {
     $.ajax({
     type: 'POST',
     url: url,
     data: form.serialize(),
     dataType: 'json',
     success: function (response) {
     if (response.status == 'ok') {
     $('.js-popupClose').click();
     swal({
     type: 'success',
     title: 'Данные отправлены',
     text: 'Спасибо за доверие',
     showConfirmButton: false,
     timer: 3000
     });
     } else {
     swal({
     type: 'error',
     title: response.message,
     text: 'Повторите еще раз',
     timer: 3000,
     showConfirmButton: false
     })
     }
     },
     error: function (xhr, str) {
     swal({
     type: 'error',
     title: 'Ошибка :(',
     text: 'Повторите еще раз',
     timer: 3000,
     showConfirmButton: false
     })
     }
     });
     }
     return false;
     });*/


    /**
     * всплывающее окно после добавления товара в корзину
     */
    function checkPopupOrderFields() {
        activeForm = $('.popup-buy-full #form-buy');
        var flag = true;
        activeForm.find('input,select').each(function () {
            if (typeof $(this).attr('required') !== 'undefined') {
                var val = $(this).val();
                if ($.trim(val) == '') {
                    flag = false;
                    $(this).focus();
                    return flag;
                }
            }
        });
        return flag;
    }

    $(document).on('submit', '.popup-buy-full #form-buy', function () {
        activeForm = $(this);
        if (activeForm.find('.notvalidate').length) {
            activeForm.find('.notvalidate').focus();
        } else {
            if (checkPopupOrderFields()) {
                $.ajax({
                    type: "POST",
                    url: activeForm.attr('action'),
                    data: activeForm.serialize(),
                    dataType: "json",
                    beforeSend: function () {
                        activeForm.addClass('loading').append('<div class="white-overlay"></div>');
                    }
                }).done(function (response) {
                    activeForm.removeClass('loading').find('.white-overlay').remove();
                    if (response.status == 'ok') {
                        yaCounter45065708.reachGoal('popupOrderSend');
                        yaCounter45065708.reachGoal('globalConversion');
                        ga('send', 'event', 'main', 'popupOrderSend');
                        ga('send', 'event', 'main', 'globalConversion');
                        swal({
                            type: 'success',
                            title: response.title,
                            text: response.message,
                            timer: 5000
                        });
                        $('.js-popupClose').click();
                    }
                    if (response.status == 'error') {
                        swal({
                            type: 'warning',
                            title: response.title,
                            text: response.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    }
                });
            }
        }
        return false;
    });

    var popupOrderAjaxUrl = '/local/ajax/basket.php';

    function makePopupAjaxRequest(data, obj) {
        $.ajax({
            url: popupOrderAjaxUrl,
            type: 'POST',
            data: data,
            dataType: 'html',
            beforeSend: function () {
                $('.popup-basket-container').addClass('loading').append('<div class="white-overlay"></div>');
            }
        }).done(function (response) {
            $('.popup-basket-container').html(response).removeClass('loading');
            if (typeof obj.fieldIndex != 'undefined') {
                var parent = $('input.field-price:eq(' + obj.fieldIndex + ')').closest('.size');
                if (parent.find('.errorNote').length) {
                    parent.find('.errorNote').remove();
                }
                var div = $('<div>').addClass('errorNote').text(obj.message);
                parent.append(div);
            }
        });
    }


    $(document).on('change', '.popup-basket-container input.field-price', function () {
        var ratio = $(this).data('ratio');
        var val = parseFloat($(this).val());
        var obj = {};
        var errorFlag = false;
        if (val < ratio && val != 0) {
            val = ratio;
            errorFlag = true;
        }

        if (val % ratio !== 0) {
            val = Math.round(val / ratio) * ratio;
            errorFlag = true;
        }

        if (errorFlag) {
            obj['fieldIndex'] = $(this).index();
            obj['message'] = "Этот товар продаётся кратно упаковкам - по " + ratio + " шт";
        }

        data = {};
        if (val <= 0) {
            data.action = 'delete';
            data.popup_basket_update = 'Y';
            data.id = parseInt($(this).attr('data-id'));
            var name = $(this).parent('tr').find('.name a').text();
            var price = $(this).parent('tr').find('.price-value').text().replace(/[^\d.]/g, '');
            analyticsDeleteBasketItem(data.id, name, price);
        } else {
            data.action = 'update';
            data.popup_basket_update = 'Y';
            data.id = parseInt($(this).attr('data-id'));
            data.quantity = parseFloat(val);
        }

        makePopupAjaxRequest(data, obj);
        return false;
    });

    /**
     * bigData click
     */

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? decodeURIComponent(matches[1]) : null;
    }

    function rememberProductRecommendation(productId) {
        // save to RCM_PRODUCT_LOG
        var cookieName = BX.cookie_prefix + '_RCM_PRODUCT_LOG',
            cookie = getCookie(cookieName),
            itemFound = false;

        var cItems = [],
            cItem;

        if (cookie) {
            cItems = cookie.split('.');
        }

        var i = cItems.length;

        while (i--) {
            cItem = cItems[i].split('-');
            if (cItem[0] == productId) {
                // it's already in recommendations, update the date
                cItem = cItems[i].split('-');

                // update rcmId and date
                cItem[1] = 'personal';
                cItem[2] = BX.current_server_time;

                cItems[i] = cItem.join('-');
                itemFound = true;
            }
            else {
                if ((BX.current_server_time - cItem[2]) > 3600 * 24 * 30) {
                    cItems.splice(i, 1);
                }
            }
        }

        if (!itemFound) {
            cItems.push([productId, 'personal', BX.current_server_time].join('-'));
        }

        // serialize
        var plNewCookie = cItems.join('.'),
            cookieDate = new Date(new Date().getTime() + 1000 * 3600 * 24 * 365 * 10).toUTCString();

        document.cookie = cookieName + "=" + plNewCookie + "; path=/; expires=" + cookieDate + "; domain=" + BX.cookie_domain;
    }

    $(document).on('click', '.card-item-image,.item-title,.list-item-image,.list-item-content__title,.table-item-image,.table-item-title,.js-add2basket', function () {
        if ($(this).closest('.bigData-slider').length) {
            var parent = $(this).closest('[data-product-id]');
            if (parent.length) {
                id = parseInt(parent.data('product-id'));
                if (id) {
                    rememberProductRecommendation(id);
                }
            }
        }
    });
});