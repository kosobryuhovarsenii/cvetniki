const gulp = require('gulp');
// Основа
const pug2html = require('./gulp/tasks/pug');
const styles = require('./gulp/tasks/styles');
const styles_clean = require('./gulp/tasks/styles_clean');

const script = require('./gulp/tasks/scripts');
// Дополнение
const fonts = require('./gulp/tasks/fonts');
const imageMinify = require('./gulp/tasks/imageMinify');
const spriteSVG = require('./gulp/tasks/spriteSVG');
const spritePNG = require('./gulp/tasks/spritePNG');
// localhost
const serve = require('./gulp/tasks/serve');
// ХЗ
const vendors = require('./gulp/tasks/vendorsJS');
const clean = require('./gulp/tasks/clean');

const dev = gulp.parallel(pug2html, script, vendors, styles, imageMinify, spriteSVG, spritePNG, fonts);

exports.default = gulp.series(
  clean,
  dev,
  // styles_clean,
  serve
);
