const gulp = require('gulp');
const plumber = require('gulp-plumber');
const scss = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const argv = require('yargs').argv;
const gulpif = require('gulp-if');
const uncss = require('gulp-uncss');
// Работаем со стилями

module.exports = function styles_clean() {
  return gulp.src('dist/static/css/style.css')
    .pipe(uncss({
      html: ['dist/*.html']
    }))
    .pipe(gulp.dest('dist/static/'))
};
