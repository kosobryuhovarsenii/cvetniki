const gulp = require('gulp');
const concat = require('gulp-concat');

const vendorsScripts = [
  // 'node_modules/svg4everybody/dist/svg4everybody.min.js',
  // 'dev/static/js/vendor/jquery-3.3.1.min.js',
  // 'dev/static/js/vendor/slick.js',
  // 'dev/static/js/vendor/jquery.maskedinput.min.js',
  // 'dev/static/js/vendor/jquery.validate.js',
];

module.exports = function vendors(cb) {
  return vendorsScripts.length
    ? gulp.src(vendorsScripts)
      .pipe(concat('libs.js'))
      .pipe(gulp.dest('dist/static/js/'))
    : cb();
};
